class App {
    App(String aTitle, String aCategory, String aDeveloper, int aYearWon){
      this.title=aTitle.toUpperCase();
      this.category=aCategory;
      this.developer=aDeveloper;
      this.yearWon=aYearWon;
    }
    late String title;
    late String category;
    late String developer;
    late int yearWon;
    }

void main() {
App fnbBankingApp = 
  App("FNB Banking App", 
      "Best iOS Consumer App", 
      "FNB Team", 
      2012);
App snapScan = 
   App("Snap Scan", 
        "Best HTML 5 app",
        "Kobus Ehlers", 
        2013);
App liveInspect = 
  App("LIVE Inspect", 
      "Best Android App",
      "Live Inspect Team", 
      2014);
App wumDrop = 
  App("Wum Drop", 
      "Best Enterprise",
      "Benjamin Claassen and Muneeb Samuels", 
      2015);
App domestly = 
  App("Domestly",
      "Best Consumer App",
      "Berno Potgieter and Thatoyaona Marumo",
      2016);
App shyft = 
  App("Shyft",
      "Best Financial Solution",
      "Brett Patrontasch",
      2017);
App khulaEcosystem = 
  App("Khula ecosystem",
      "Best Agriculture Solution",
      "Karidas Tshintsholo, Matthew Piper and Jackson Dyora",
      2018);
App nakedInsurance = 
  App("Naked Insurance",
      "Best Financial Solution",
      "Sumarie Greybe, Ernest North and Alex Thomson",
      2019);
App easyEquities = 
  App("EasyEquities",
      "Best Consumer Solution",
      "Charles Savage",
      2020);
App ambaniAfrica = 
  App("Ambani Africa",
      "Best Gaming Solution, Best Educational, and Best South African Solution.",
      "Mukundi Lambani",
      2021);

print("2012 App of the Year");
  print(fnbBankingApp.title);
    print(fnbBankingApp.category);
      print(fnbBankingApp.developer);
        print(fnbBankingApp.yearWon);
          print("");
print("2013 App of the Year");
  print(snapScan.title);
    print(snapScan.category);
      print(snapScan.developer);
        print(snapScan.yearWon);
          print("");
print("2014 App of the Year");
  print(liveInspect.title);
    print(liveInspect.category);
      print(liveInspect.developer);
        print(liveInspect.yearWon);
          print("");
print("2015 App of the Year");
  print(wumDrop.title);
    print(wumDrop.category);
      print(wumDrop.developer);
        print(wumDrop.yearWon);
          print("");
print("2016 App of the Year");
  print(domestly.title);
    print(domestly.category);
      print(domestly.developer);
        print(domestly.yearWon);
          print("");
print("2017 App of the Year");
  print(shyft.title);
    print(shyft.category);
      print(shyft.developer);
        print(shyft.yearWon);
          print("");
print("2018 App of the Year");
  print(khulaEcosystem.title);
    print(khulaEcosystem.category);
      print(khulaEcosystem.developer);
        print(khulaEcosystem.yearWon);
          print("");
print("2019 App of the Year");
  print(nakedInsurance.title);
    print(nakedInsurance.category);
      print(nakedInsurance.developer);
        print(nakedInsurance.yearWon);
          print("");
print("2020 App of the Year");
  print(easyEquities.title);
    print(easyEquities.category);
      print(easyEquities.developer);
        print(easyEquities.yearWon);
          print("");
print("2021 App of the Year");
  print(ambaniAfrica.title);
    print(ambaniAfrica.category);
      print(ambaniAfrica.developer);
        print(ambaniAfrica.yearWon);
          print("");

}